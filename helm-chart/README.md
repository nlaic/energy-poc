# Energy PoC Helm Charts

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

Umbrella chart for a Energy PoC

## Deployment content
This umbrella chart deploys the following components using Helm charts:
- IDS broker
- IDS daps
- MongoDb instance
- IDS connectors for Netbeheerder, BAG and the IDS consumer
- UI and API for the OpenData and SAML consumer
- SAML IdP

### Install command
Make sure to install the Helm chart using
```
helm install energy-poc . -f values.yaml -f values.bag.yaml -f values.gridoperator.yaml -f values.idsconsumer.yaml
```
to install the connectors for BAG, Netbeheerder, IDS Consumer and the UI for the SAML and OpenData consumer.

### IDS Connectors
Each connector is deployed as a dependency described in ``Chart.yaml``, where an alias is used to give it it's instance name.
For each of those instances, a separate values file (like `values.bag.yaml`) is present.

## Configuration values
Most of the values present in the `values*.yaml` files are configured so that all services are connected.
The following values are important to change:
- **mongoDbPassword**: password used to connect to the mongodb instance
- **host**: (in each of the `values.*.yaml`) URL to reach a specific connector.

Documentation and explanaion for values are present in `values.bag.yaml`.
gridoperator, idsconsumer use similar configuration values.

## Docker Registry Pull Secret
For the PoC deployment to work a Docker Pull Secret with the name `ids-pull-secret` has to be present on the Kubernetes cluster in the namespace you'd want to deploy the PoC.

Contact [Maarten Kollenstart](mailto:maarten.kollenstart@tno.nl?SUBJECT=NL%20AIC-PoCs%20-%20Credentials) for more information and the actual credentials.
