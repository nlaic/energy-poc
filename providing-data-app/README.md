# Providing Data App

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains Java code that implements an IDS Data App.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

## Introduction

The Producing Data App is used receive resource requests from the [Consuming Data App](../consuming-data-app/READM.md), collect the requested data conform the identity used and return the data.

## Building the project

Gradle is used as build tool, build the project by running ``./gradlew build``.

## Configuration

The Data App needs some YAML configuration values placed in ``/ids/config.yaml`` to work properly:

| Key                              | Description                                                 |
| -------------------------------- | ----------------------------------------------------------- |
| id                               | URN of an ID                                                |
| participant                      | URN of the participant                                      |
