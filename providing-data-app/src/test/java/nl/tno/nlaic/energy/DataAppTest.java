package nl.tno.nlaic.energy;

import nl.tno.ids.common.config.dataapp.DataAppConfig;
import nl.tno.ids.common.serialization.Config;
import nl.tno.nlaic.energy.provider.Application;
import nl.tno.nlaic.energy.provider.model.InitMongoData;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(properties = { "spring.data.mongodb.uri=mongodb://root:password@127.0.0.1:27017/admin" })
@Ignore
public class DataAppTest {
  @Autowired
  InitMongoData initMongoData;

  @BeforeClass
  public static void beforeClass() {
    String file = DataAppTest.class.getClassLoader().getResource("config.yaml").getFile();
    DataAppConfig dataAppConfig = Config.dataApp(file);
  }

  @Test
  public void test() throws InterruptedException {
//    initMongoData.init_data();
//    initMongoData.run();
    while(true) {
      Thread.sleep(1000);
    }
  }

}
