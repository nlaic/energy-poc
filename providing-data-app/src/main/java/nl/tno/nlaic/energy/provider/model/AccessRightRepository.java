package nl.tno.nlaic.energy.provider.model;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccessRightRepository extends MongoRepository<AccessRight, String> {

    public List<AccessRight> findAll();

    public List<AccessRight> findByName(String name);

    public List<AccessRight> findByAccessLevelGreaterThan(int accessLevel);

    public List<AccessRight> findByAccessLevelLessThan(int accessLevel);
}