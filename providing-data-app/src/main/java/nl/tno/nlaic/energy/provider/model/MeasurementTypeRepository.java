package nl.tno.nlaic.energy.provider.model;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MeasurementTypeRepository extends MongoRepository<MeasurementType, String> {

    public List<MeasurementType> findAll();

    public List<MeasurementType> findByName(String name);

    public List<MeasurementType> findByAccessRightIn(List<AccessRight> accessRights);
    
}