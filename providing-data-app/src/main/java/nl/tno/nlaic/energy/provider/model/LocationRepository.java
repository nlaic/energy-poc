package nl.tno.nlaic.energy.provider.model;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LocationRepository extends MongoRepository<Location, String> {

    public List<Location> findByLocationType(LocationType locationType);

    public List<Location> findByLocationTypeIn(List<LocationType> locationTypes);

    public List<Location> findAll();

    public List<Location> findByName(String name);

    public List<Location> findByParent(Location parent);

}