package nl.tno.nlaic.energy.provider;

import brave.Tracing;
import brave.httpclient.TracingHttpClientBuilder;
import de.fraunhofer.iais.eis.Message;
import de.fraunhofer.iais.eis.RejectionReason;
import nl.tno.ids.base.HttpHelper;
import nl.tno.ids.base.IDSRestController;
import nl.tno.ids.base.ResponseBuilder;
import nl.tno.ids.common.multipart.MultiPartMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class IDSController extends IDSRestController {
    @Autowired(required = false)
    private Tracing tracing;

    @Autowired(required = true)
    private DBHandler dBHandler;

    private static final Logger LOG = LoggerFactory.getLogger(IDSController.class);

    @PostConstruct
    public void postConstruct() {
        LOG.info("Creating tracing httpclient");
        
        if (tracing == null) {
            tracing = Tracing.newBuilder().build();
        }
        HttpHelper.getHttpClient(TracingHttpClientBuilder.create(tracing));
        LOG.info("Created tracing httpclient");

        EnergyMessageHandler energyMessageHandler = new EnergyMessageHandler(dBHandler);
        this.registerMessageHandler(energyMessageHandler);
    }

    public static MultiPartMessage createRejectionEntity(Message message, RejectionReason reason) {
        return new MultiPartMessage.Builder()
            .setHeader(ResponseBuilder.createRejectionMessage(message, reason).build())
            .build();
    }
    public static MultiPartMessage createRejectionEntity(Message message, RejectionReason reason, String explanation) {
        return new MultiPartMessage.Builder()
            .setHeader(ResponseBuilder.createRejectionMessage(message, reason).build())
            .setPayload(explanation)
            .build();
    }

}
