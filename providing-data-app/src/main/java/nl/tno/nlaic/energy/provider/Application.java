/*
 * Copyright 2016 Fraunhofer-Institut für Materialfluss und Logistik IML und Fraunhofer-Institut für Software- und Systemtechnik ISST
 * Industrial Data Space, Reference Use Case Logistics
 *
 * For license information or if you have any questions contact us at ids-ap2@isst.fraunhofer.de.
 */
package nl.tno.nlaic.energy.provider;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.sleuth.instrument.async.LazyTraceExecutor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Role;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * This is a Spring Boot Application.
 * @see <a href="http://projects.spring.io/spring-boot/">Spring Boot</a>
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class,
    DataSourceTransactionManagerAutoConfiguration.class,
    HibernateJpaAutoConfiguration.class,
    AopAutoConfiguration.class,
//    SamplerAutoConfiguration.class
})
@ComponentScan("nl.tno.nlaic")
@EnableAsync
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class Application implements AsyncConfigurer {
  @Autowired
  private BeanFactory beanfactory;


  public static void main(String[] args) {
    
    SpringApplication.run(Application.class, args);
  }

  public Application() {
    
  }

  @Override
  public Executor getAsyncExecutor() {
    LoggerFactory.getLogger(Application.class).info("Creating LazyTraceExecutor");
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(5);
    executor.setMaxPoolSize(10);
    executor.setKeepAliveSeconds(5);
    executor.initialize();
    return new LazyTraceExecutor(this.beanfactory, executor);
  }
}
