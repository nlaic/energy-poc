package nl.tno.nlaic.energy.provider.exceptions;
import org.springframework.http.HttpStatus;

public class CustomException extends Exception{
   
    protected HttpStatus status = HttpStatus.UNAUTHORIZED;

    public CustomException(String errorMessage) {
        super(errorMessage);
    }
    
    public HttpStatus getHttpStatus() {
        return status;
    }
}
