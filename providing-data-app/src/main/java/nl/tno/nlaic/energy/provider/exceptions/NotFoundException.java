package nl.tno.nlaic.energy.provider.exceptions;

import org.springframework.http.HttpStatus;

public class NotFoundException extends CustomException{

     public NotFoundException(String errorMessage) {
        super(errorMessage);
        status = HttpStatus.NOT_FOUND;
    }
}
