package nl.tno.nlaic.energy.provider.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.data.annotation.Id;

public class MeasurementType  {

    @JsonProperty("id")
    @Id
    public String id;

    @JsonProperty("accessRight")
    private AccessRight accessRight;

    @JsonProperty("name")
    private String name = null;

    public MeasurementType(){
    }
    
    public MeasurementType(String id, AccessRight accessRight, String name) {
        this.id = id;
        this.accessRight = accessRight;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AccessRight getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(AccessRight accessRight) {
        this.accessRight = accessRight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "MeasurementType [accessLevel=" + accessRight + ", id=" + id + ", name=" + name + "]";
    }

}
