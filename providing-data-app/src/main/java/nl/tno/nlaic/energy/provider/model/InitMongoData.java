package nl.tno.nlaic.energy.provider.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class InitMongoData implements Runnable {
  private static final Logger LOG = LoggerFactory.getLogger(InitMongoData.class);

  @Autowired
  private AccessRightRepository arRepository;
  @Autowired
  private LocationRepository lRepository;
  @Autowired
  private LocationTypeRepository ltRepository;
  @Autowired
  private MeasurementRepository mRepository;
  @Autowired
  private MeasurementTypeRepository mtRepository;

  Map<String, Location> locations = new HashMap<String, Location>();
  Map<String, AccessRight> accessRights = new HashMap<String, AccessRight>();
  Map<String, LocationType> locationsTypes = new HashMap<String, LocationType>();
  Map<String, MeasurementType> measurementTypes = new HashMap<String, MeasurementType>();
  List<Measurement> measurements = new ArrayList<>();
  Random r = new Random();

  public void init_data() {
    LOG.info("Initializing MongoDB");
    AccessRight idsRight = new AccessRight();
    idsRight.setName("IDS");
    idsRight.setAccessLevel(3);
    arRepository.save(idsRight);
    accessRights.put(idsRight.getName(), idsRight);

    AccessRight samlRight = new AccessRight();
    samlRight.setName("SAML");
    samlRight.setAccessLevel(2);
    arRepository.save(samlRight);
    accessRights.put(samlRight.getName(), samlRight);

    AccessRight openRight = new AccessRight();
    openRight.setName("OPEN");
    openRight.setAccessLevel(1);
    arRepository.save(openRight);
    accessRights.put(openRight.getName(), openRight);

    LocationType locationTypeCountry = new LocationType();
    locationTypeCountry.setName("country");
    locationTypeCountry.setAccessRight(openRight);
    ltRepository.save(locationTypeCountry);
    locationsTypes.put(locationTypeCountry.getName(), locationTypeCountry);

    LocationType locationTypeCity = new LocationType();
    locationTypeCity.setName("city");
    locationTypeCity.setAccessRight(openRight);
    ltRepository.save(locationTypeCity);
    locationsTypes.put(locationTypeCity.getName(), locationTypeCity);

    LocationType locationTypeStreet = new LocationType();
    locationTypeStreet.setName("street");
    locationTypeStreet.setAccessRight(idsRight);
    ltRepository.save(locationTypeStreet);
    locationsTypes.put(locationTypeStreet.getName(), locationTypeStreet);

    LocationType locationTypePostalRange = new LocationType();
    locationTypePostalRange.setName("postalRange");
    locationTypePostalRange.setAccessRight(idsRight);
    ltRepository.save(locationTypePostalRange);
    locationsTypes.put(locationTypePostalRange.getName(), locationTypePostalRange);

    MeasurementType ELK_SJV_GEMIDDELD = new MeasurementType();
    ELK_SJV_GEMIDDELD.setName("ELK_SJV_AVERAGE");
    ELK_SJV_GEMIDDELD.setAccessRight(samlRight);
    mtRepository.save(ELK_SJV_GEMIDDELD);
    measurementTypes.put(ELK_SJV_GEMIDDELD.getName(), ELK_SJV_GEMIDDELD);

    MeasurementType GAS_SJV_GEMIDDELD = new MeasurementType();
    GAS_SJV_GEMIDDELD.setName("GAS_SJV_AVERAGE");
    GAS_SJV_GEMIDDELD.setAccessRight(openRight);
    mtRepository.save(GAS_SJV_GEMIDDELD);
    measurementTypes.put(GAS_SJV_GEMIDDELD.getName(), GAS_SJV_GEMIDDELD);

    MeasurementType ELK_SJV_LAAG_TARIEF_PERC = new MeasurementType();
    ELK_SJV_LAAG_TARIEF_PERC.setName("ELK_SJV_LOW_TARIF_PERC");
    ELK_SJV_LAAG_TARIEF_PERC.setAccessRight(idsRight);
    mtRepository.save(ELK_SJV_LAAG_TARIEF_PERC);
    measurementTypes.put(ELK_SJV_LAAG_TARIEF_PERC.getName(), ELK_SJV_LAAG_TARIEF_PERC);
    LOG.info("Initializing finished");
  }

  public void run() {

    LOG.info("Starting CSV import");
    BufferedReader reader = null;
    try {

      InputStream inputStream = InitMongoData.class.getClassLoader().getResource("input_data.csv").openStream();
      reader = new BufferedReader(new InputStreamReader(inputStream));
      reader.readLine(); // read away forst line with headers
      int i = 0;
      String line = null;
      while ((line = reader.readLine()) != null) {
        i++;
        if (i % 10000 == 0) {
          LOG.info("Done {} lines", i);
        }
        String[] item = line.split(";"); // csv file is "" separated


        String country = item[6];
        Location countryLocation = locations.get(country);
        if (countryLocation == null) {
          countryLocation = new Location();
          countryLocation.setName(item[6]);
          countryLocation.setLocationType(locationsTypes.get("country"));
//                    lRepository.save(countryLocation);
          locations.put(country, countryLocation);
        }

        String city = item[5];
        Location cityLocation = locations.get(city);
        if (cityLocation == null) {
          cityLocation = new Location();
          cityLocation.setName(item[5]);
          cityLocation.setLocationType(locationsTypes.get("city"));
          cityLocation.setParent(countryLocation);
//                    lRepository.save(cityLocation);
          locations.put(city, cityLocation);
        }

        String streetName = item[2];
        Location streetLocation = locations.get(streetName);
        if (streetLocation == null) {
          streetLocation = new Location();
          streetLocation.setName(item[2]);
          streetLocation.setLocationType(locationsTypes.get("street"));
          streetLocation.setParent(cityLocation);
//                    lRepository.save(streetLocation);
          locations.put(streetName, streetLocation);
        }


        String postalArea = item[3] + " tot " + item[4];
        Location postalLocation = locations.get(postalArea);
        if (postalLocation == null) {
          postalLocation = new Location();
          postalLocation.setName(item[3] + " tot " + item[4]);
          postalLocation.setParent(streetLocation);
          postalLocation.setLocationType(locationsTypes.get("postalRange"));
//                    lRepository.save(postalLocation);
          locations.put(postalArea, postalLocation);
        }

        if ("GAS".equals(item[7])) {

          Measurement GAS_SJV_average = new Measurement();
          GAS_SJV_average.setLocation(postalLocation);
          GAS_SJV_average.setDate(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
          GAS_SJV_average.setMeasurementType(measurementTypes.get("GAS_SJV_AVERAGE"));
          GAS_SJV_average.setValue(Double.parseDouble(item[14].replaceAll(",", ".")));
          GAS_SJV_average.setNumberOfSensors(Integer.parseInt(item[9]));
          measurements.add(GAS_SJV_average);

//                    updateAverages(GAS_SJV_average);

        } else if ("ELK".equals(item[7])) {
          Measurement ELK_SJV_average = new Measurement();
          ELK_SJV_average.setLocation(postalLocation);
          ELK_SJV_average.setDate(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
          ELK_SJV_average.setMeasurementType(measurementTypes.get("ELK_SJV_AVERAGE"));
          ELK_SJV_average.setValue(Double.parseDouble(item[14].replaceAll(",", ".")));
          ELK_SJV_average.setNumberOfSensors(Integer.parseInt(item[9]));
          measurements.add(ELK_SJV_average);
//                    updateAverages(ELK_SJV_average);

          Measurement ELK_SJV_low_prec = new Measurement();
          ELK_SJV_low_prec.setLocation(postalLocation);
          ELK_SJV_low_prec.setDate(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
          ELK_SJV_low_prec.setMeasurementType(measurementTypes.get("ELK_SJV_LOW_TARIF_PERC"));
          ELK_SJV_low_prec.setValue(Double.parseDouble(item[15].replaceAll(",", ".")));
          ELK_SJV_low_prec.setNumberOfSensors(Integer.parseInt(item[9]));
          measurements.add(ELK_SJV_low_prec);

//                    updateAverages(ELK_SJV_low_prec);

          double randomValue = 60 + (200 - 60) * r.nextDouble();
          postalLocation.setTotalSurfaceArea(randomValue * Integer.parseInt(item[9]));
//                    lRepository.save(postalLocation);

//                    updateParentsSurfaceArea(postalLocation);
        }
      }
      LocationType postalRange = locationsTypes.get("postalRange");
      locations.values().stream().filter(location -> location.getLocationType() == postalRange).forEach(this::updateParentsSurfaceArea);

      LOG.info("Postal info measurements: {}", measurements.size());
      List<Measurement> streetMeasurements = getParentMeasurements(measurements);
      LOG.info("Street measurements: {}", streetMeasurements.size());
      List<Measurement> cityMeasurements = getParentMeasurements(streetMeasurements);
      LOG.info("City measurements: {}", cityMeasurements.size());
      List<Measurement> countryMeasurements = getParentMeasurements(cityMeasurements);
      LOG.info("Country measurements: {}", countryMeasurements.size());
      LOG.info("Total measurements: {}", measurements.size());
      int batchsize = 10000;
      ArrayList<Location> locationsCopy = new ArrayList<>(locations.values());
      for (int j = 0; j < Math.ceil(locations.size() / (float) batchsize); j++) {
        LOG.info("Storing locations: {}/{}", j*batchsize, locations.size());
        lRepository.saveAll(locationsCopy.subList(j*batchsize, Math.min((j+1)*batchsize, locations.size())));
      }
      ArrayList<Measurement> measurementsCopy = new ArrayList<>(measurements);
      for (int j = 0; j < Math.floor(measurements.size() / (float) batchsize); j++) {
        LOG.info("Storing measurements: {}/{}", j*batchsize, measurements.size());
        mRepository.saveAll(measurementsCopy.subList(j*batchsize, Math.min((j+1)*batchsize, measurements.size())));
      }
      locations.values().forEach(lRepository::save);
      measurements.forEach(mRepository::save);
      // Save all give Java heap space error
      // lRepository.saveAll(locations.values());
      // mRepository.saveAll(measurements);

    } catch (Exception e) {
      e.printStackTrace();

    } finally {
      if (reader != null)
        try {
          reader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
    }
    LOG.info("Finished CSV import");
  }
//    static <T,K> Collector<T,?,Map<K,List<T>>> groupingBy(Function<? super T,? extends K> classifier);

  private List<Measurement> getParentMeasurements(List<Measurement> measurementList) {
    return measurementList.stream()
        .filter(measurement -> measurement.getLocation().getParent() != null)
        .collect(Collectors.groupingBy(measurement -> measurement.getLocation().getParent()))
        .entrySet()
        .stream()
        .map(parentEntry -> {
          return parentEntry.getValue().stream()
              .collect(Collectors.groupingBy(Measurement::getMeasurementType))
              .entrySet()
              .stream()
              .map(typeEntry -> {
                try {
                  Measurement parentMeasurement = new Measurement();
                  parentMeasurement.setLocation(parentEntry.getValue().get(0).getLocation().getParent());
                  parentMeasurement.setDate(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
                  parentMeasurement.setMeasurementType(typeEntry.getKey());
                  parentMeasurement.setValue(typeEntry.getValue().stream().mapToDouble(Measurement::getValue).sum());
                  measurements.add(parentMeasurement);
                  return parentMeasurement;
                } catch (ParseException e) {
                  e.printStackTrace();
                }
                return null;

              }).collect(Collectors.toList());
        }).flatMap(List::stream).collect(Collectors.toList());
  }

  public void dropCollections() throws UnknownHostException {
    LOG.info("Dropping collections");
    arRepository.deleteAll();
    lRepository.deleteAll();
    ltRepository.deleteAll();
    mRepository.deleteAll();
    mtRepository.deleteAll();
  }

  private void updateParentsSurfaceArea(Location location) {
    Location parentLocation = location.getParent();
    while (parentLocation != null) {

      parentLocation.setTotalSurfaceArea(parentLocation.getTotalSurfaceArea() + location.getTotalSurfaceArea());
//            lRepository.save(parentLocation);
      parentLocation = parentLocation.getParent();
    }
  }


//  private void updateAverages(Measurement measurement) throws ParseException {
//
//    Location parentLocation = measurement.getLocation().getParent();
//
//    while (parentLocation != null) {
//      measurements.get(parentLocation.getId());
//      List<Measurement> parentMeasurements = mRepository.findByLocationAndMeasurementType(
//          parentLocation, measurement.getMeasurementType());
//      Measurement parentMeasurement = null;
//      if (parentMeasurements.isEmpty()) {
//        parentMeasurement = new Measurement();
//        parentMeasurement.setLocation(measurement.getLocation().getParent());
//        parentMeasurement.setDate(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01"));
//        parentMeasurement.setMeasurementType(measurement.getMeasurementType());
//        parentMeasurement.setValue(measurement.getValue());
//        mRepository.save(parentMeasurement);
//      } else {
//        parentMeasurement = parentMeasurements.get(0);
//        double average = ((parentMeasurement.getNumberOfSensors() * parentMeasurement.getValue())
//            +
//            (measurement.getNumberOfSensors() * measurement.getValue()))
//            /
//            (measurement.getNumberOfSensors() + parentMeasurement.getNumberOfSensors());
//        parentMeasurement.setNumberOfSensors(parentMeasurement.getNumberOfSensors() + measurement.getNumberOfSensors());
//        parentMeasurement.setValue(average);
//        mRepository.save(parentMeasurement);
//      }
//      parentLocation = parentMeasurement.getLocation().getParent();
//
//      measurement = parentMeasurement;
//    }
//  }

  public InitMongoData() {
  }
}
