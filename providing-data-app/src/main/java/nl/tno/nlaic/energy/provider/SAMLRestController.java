package nl.tno.nlaic.energy.provider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.tno.ids.common.serialization.Config;
import nl.tno.nlaic.energy.provider.exceptions.AccessRightToLowException;
import nl.tno.nlaic.energy.provider.exceptions.NotFoundException;
import nl.tno.nlaic.energy.provider.model.AccessRight;
import nl.tno.nlaic.energy.provider.model.Location;
import nl.tno.nlaic.energy.provider.model.LocationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;


@RestController
@RequestMapping("/saml")
public class SAMLRestController {

  @Autowired
  DBHandler dBHandler;

  private static final Logger LOG = LoggerFactory.getLogger(SAMLRestController.class);
  private AccessRight accessRight;
  private static ObjectMapper objectMapper = new ObjectMapper();
  boolean measurementProviderType = Config.dataApp().getCustomProperties().get("measurementProvider").asBoolean();

  @PostConstruct
  public void postConstruct() {
    accessRight = dBHandler.getFirstAccessRightByName("SAML");
    
  }

  @RequestMapping(value = "/data-endpoint/locations", method = RequestMethod.GET)
  public ResponseEntity getLocations() throws JsonProcessingException {
    if (measurementProviderType) {
      LOG.info("Not found: /data-endpoint/locations");
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(objectMapper.writeValueAsString(dBHandler.getLocations(accessRight)));
  }

  @RequestMapping(value = "/data-endpoint/locations/locationtypes", method = RequestMethod.GET)
  public ResponseEntity getLocationTypes() throws JsonProcessingException {
    if (measurementProviderType) {
      LOG.info("Not found: /data-endpoint/locations/locationtypes");
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(objectMapper.writeValueAsString(dBHandler.getLocationTypes(accessRight)));
  }

  @RequestMapping(value = "/data-endpoint/locations/{id}", method = RequestMethod.GET)
  public ResponseEntity getLocationByID(@PathVariable("id") String id) throws JsonProcessingException {
    if (measurementProviderType) {
      LOG.info("Not found: /data-endpoint/locations/{}", id);
      return ResponseEntity.notFound().build();
    }
    try {
      Location location = dBHandler.getLocationById(accessRight, id);
      return ResponseEntity
          .ok(objectMapper.writeValueAsString(dBHandler.getMeasurementsByLocation(accessRight, location)));
    } catch (AccessRightToLowException | NotFoundException e) {
      LOG.info("Error in retrieving data: {} -> {}", e.getHttpStatus(), e.getMessage());
      return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }
  }
  @RequestMapping(value = "/data-endpoint/locations/{id}/children", method = RequestMethod.GET)
  public ResponseEntity getLocationsByParent(@PathVariable("id") String id) throws JsonProcessingException {
    if (measurementProviderType) {
      LOG.info("Not found: /data-endpoint/locations/{}/children", id);
      return ResponseEntity.notFound().build();
    }
    try {
      return ResponseEntity.ok(objectMapper.writeValueAsString(dBHandler.getLocationsByParent(accessRight, id)));
    } catch (AccessRightToLowException | NotFoundException e) {
      LOG.info("Error in retrieving data: {} -> {}", e.getHttpStatus(), e.getMessage());
      return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }
  }

  @RequestMapping(value = "/data-endpoint/locations/locationtypes/{id}", method = RequestMethod.GET)
  public ResponseEntity getLocationsByLocationType(@PathVariable("id") String id) throws JsonProcessingException {
    if (measurementProviderType) {
      LOG.info("Not found: /data-endpoint/locations/locationtypes/{}", id);
      return ResponseEntity.notFound().build();
    }
    try {
      LocationType locationType = dBHandler.getLocationTypeById(accessRight, id);
      return ResponseEntity
          .ok(objectMapper.writeValueAsString(dBHandler.getLocationsByLocationType(accessRight, locationType)));
    } catch (AccessRightToLowException | NotFoundException e) {
      LOG.info("Error in retrieving data: {} -> {}", e.getHttpStatus(), e.getMessage());
      return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }
  }

  @RequestMapping(value = "/data-endpoint/locations/locationtypes/{id}/measurements", method = RequestMethod.GET)
  public ResponseEntity getMeasurementsByLocationType(@PathVariable("id") String id) throws JsonProcessingException {
    if (!measurementProviderType) {
      LOG.info("Not found: /data-endpoint/locations/locationtypes/{}/measurements", id);
      return ResponseEntity.notFound().build();
    }
    try {
      LocationType locationType = dBHandler.getLocationTypeById(accessRight, id);
      List<Location> locations = dBHandler.getLocationsByLocationType(accessRight, locationType);
      if (!locations.isEmpty())
        return ResponseEntity
            .ok(objectMapper.writeValueAsString(dBHandler.getMeasurementsByLocations(accessRight, locations)));
      else
        return ResponseEntity.ok("{}");
    } catch (AccessRightToLowException | NotFoundException e) {
      LOG.info("Error in retrieving data: {} -> {}", e.getHttpStatus(), e.getMessage());
      return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }
  }

  @RequestMapping(value = "/data-endpoint/locations/{id}/measurements", method = RequestMethod.GET)
  public ResponseEntity getMeasurementByLocation(@PathVariable("id") String id) throws JsonProcessingException {
    if (!measurementProviderType) {
      LOG.info("Not found: data-endpoint/locations/{}/measurements", id);
      return ResponseEntity.notFound().build();
    }
    try {
      Location location = dBHandler.getLocationById(accessRight, id);
      return ResponseEntity
          .ok(objectMapper.writeValueAsString(dBHandler.getMeasurementsByLocation(accessRight, location)));
    } catch (AccessRightToLowException | NotFoundException e) {
      LOG.info("Error in retrieving data: {} -> {}", e.getHttpStatus(), e.getMessage());
      return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }
  }
}