package nl.tno.nlaic.energy.provider.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class AccessRight {

    @JsonProperty("id")
    @Id
    public String id;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("accessLevel")
    private int accessLevel = 0;

    public AccessRight() {
    }
    
    public AccessRight(String id, String name, int accessLevel) {
        this.id = id;
        this.name = name;
        this.accessLevel = accessLevel;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AccessRight {accessLevel=" + accessLevel + ", id=" + id + ", name=" + name + "}";
    }

}
