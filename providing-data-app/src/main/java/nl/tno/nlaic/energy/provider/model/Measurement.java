package nl.tno.nlaic.energy.provider.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class Measurement  {

    @JsonProperty("id")
    @Id
    public String id;

    @JsonProperty("date")
    private Date date = null;

    @JsonProperty("value")
    private double value = 0;

    @JsonProperty("location")
    private Location location = null;

    @JsonProperty("type")
    private MeasurementType measurementType = null;

    @JsonProperty("numberOfSensors")
    private int numberOfSensors = 0;

    public Measurement() {
    }

    public Measurement(String id, Date date, double value, Location location, MeasurementType measurementType) {
        this.id = id;
        this.date = date;
        this.value = value;
        this.location = location;
        this.measurementType = measurementType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public MeasurementType getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(MeasurementType measurementType) {
        this.measurementType = measurementType;
    }

    public int getNumberOfSensors() {
        return numberOfSensors;
    }

    public void setNumberOfSensors(int numberOfSensors) {
        this.numberOfSensors = numberOfSensors;
    }
    
    @Override
    public String toString() {
        return "Measurement {date=" + date + ", id=" + id + ", location=" + location + ", measurementType=" + measurementType + ", value="
                + value + "}";
    }
    
}
