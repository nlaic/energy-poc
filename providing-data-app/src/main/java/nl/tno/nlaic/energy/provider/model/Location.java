package nl.tno.nlaic.energy.provider.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class Location {

    @JsonProperty("id")
    @Id
    public String id;

    @JsonProperty("locationType")
    private LocationType locationType = null;

    @JsonProperty("totalSurfaceArea")
    private double totalSurfaceArea = 0;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("parent")
    private Location parent = null;

    public Location() {
    }

    public Location(String id, LocationType locationType, String name, Location parent) {
        this.id = id;
        this.locationType = locationType;
        this.name = name;
        this.parent = parent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getParent() {
        return parent;
    }

    public void setParent(Location parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "Location {id=" + id + ", name=" + name + ", parent=" + parent + ", locationType=" + locationType + "}";
    }

    public double getTotalSurfaceArea() {
        return totalSurfaceArea;
    }

    public void setTotalSurfaceArea(double totalSurfaceArea) {
        this.totalSurfaceArea = totalSurfaceArea;
    }

}
