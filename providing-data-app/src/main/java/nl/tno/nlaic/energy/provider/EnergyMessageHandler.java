package nl.tno.nlaic.energy.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iais.eis.ArtifactRequestMessage;
import de.fraunhofer.iais.eis.ArtifactResponseMessageBuilder;
import nl.tno.ids.base.MessageHandler;
import nl.tno.ids.common.multipart.MultiPart;
import nl.tno.ids.common.multipart.MultiPartMessage;
import nl.tno.ids.common.serialization.Config;
import nl.tno.ids.common.serialization.DateUtil;
import nl.tno.nlaic.energy.provider.exceptions.AccessRightToLowException;
import nl.tno.nlaic.energy.provider.exceptions.NotFoundException;
import nl.tno.nlaic.energy.provider.model.AccessRight;
import nl.tno.nlaic.energy.provider.model.Location;
import nl.tno.nlaic.energy.provider.model.LocationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.fraunhofer.iais.eis.util.Util.asList;

public class EnergyMessageHandler implements MessageHandler<ArtifactRequestMessage> {
  private static final Logger LOG = LoggerFactory.getLogger(EnergyMessageHandler.class);
  private final DBHandler dBHandler;
  AccessRight accessRight = null;
  private static final ObjectMapper objectMapper = new ObjectMapper();
  private static final boolean measurementProviderType = Config.dataApp().getCustomProperties().get("measurementProvider").asBoolean();
  private static final Pattern locations = Pattern.compile(".*locations$");
  private static final Pattern locationById = Pattern.compile(".*locations/(?<locationId>[0-9a-zA-Z]+)$");
  private static final Pattern locationByParent = Pattern.compile(".*locations/(?<locationId>[0-9a-zA-Z]+)/children$");
  private static final Pattern measurementsById = Pattern.compile(".*locations/(?<locationId>[0-9a-zA-Z]+)/measurements$");
  private static final Pattern types = Pattern.compile(".*locations/locationtypes$");
  private static final Pattern typeById = Pattern.compile(".*locations/locationtypes/(?<locationType>[0-9a-zA-Z]+)$");
  private static final Pattern measurementsByType = Pattern.compile(".*locations/locationtypes/(?<locationType>[0-9a-zA-Z]+)/measurements$");

  public EnergyMessageHandler(DBHandler dBHandler) {
    super();
    this.dBHandler = dBHandler;
  }

  @Override
  public ResponseEntity handle(ArtifactRequestMessage header, String payload) throws IOException {
    if (accessRight == null) {
      accessRight = dBHandler.getFirstAccessRightByName("IDS");
    }
    URI requestedArtifact = header.getRequestedArtifact();
    String urlPath = requestedArtifact.getPath();

    HttpStatus status = HttpStatus.OK;
    String payloadString = "";
    try {
      if (measurementProviderType) {
        Matcher m1 = measurementsById.matcher(urlPath);
        Matcher m2 = measurementsByType.matcher(urlPath);
        if (m1.find()) {
          LOG.info("Get location measurements: {}", m1.group("locationId"));
          Location location = dBHandler.getLocationById(accessRight, m1.group("locationId"));
          payloadString = objectMapper.writeValueAsString(dBHandler.getMeasurementsByLocation(accessRight, location));
        } else if (m2.find()) {
          LOG.info("Get locationtype measurements: {}", m2.group("locationType"));
          LocationType locationType = dBHandler.getLocationTypeById(accessRight, m2.group("locationType"));
          List<Location> locations = dBHandler.getLocationsByLocationType(accessRight, locationType);
          if (!locations.isEmpty()) {
            payloadString = objectMapper.writeValueAsString(dBHandler.getMeasurementsByLocations(accessRight, locations));
          } else {
            payloadString = "{}";
          }
        } else {
          LOG.info("Could not map {}", urlPath);
          status = HttpStatus.NOT_FOUND;
        }
      } else {
        Matcher m1 = locations.matcher(urlPath);
        Matcher m2 = types.matcher(urlPath);
        Matcher m3 = locationById.matcher(urlPath);
        Matcher m4 = locationByParent.matcher(urlPath);
        Matcher m5 = typeById.matcher(urlPath);
        if (m1.find()) {
          LOG.info("Get locations");
          payloadString = objectMapper.writeValueAsString(dBHandler.getLocations(accessRight));
        } else if (m2.find()) {
          LOG.info("Get location types");
          payloadString = objectMapper.writeValueAsString(dBHandler.getLocationTypes(accessRight));
        } else if (m3.find()) {
          LOG.info("Get location: {}", m3.group("locationId"));
          Location location = dBHandler.getLocationById(accessRight, m3.group("locationId"));
          payloadString = objectMapper.writeValueAsString(location);
        } else if (m4.find()) {
          LOG.info("Get locations by parent: {}", m4.group("locationId"));
          List<Location> locations = dBHandler.getLocationsByParent(accessRight, m4.group("locationId"));
          payloadString = objectMapper.writeValueAsString(locations);
        } else if (m5.find()) {
          LOG.info("Get location type: {}", m5.group("locationType"));
          LocationType locationType = dBHandler.getLocationTypeById(accessRight, m5.group("locationType"));
          payloadString = objectMapper.writeValueAsString(dBHandler.getLocationsByLocationType(accessRight, locationType));
        } else {
          LOG.info("Could not map {}", urlPath);
          status = HttpStatus.NOT_FOUND;
        }
      }
    } catch (AccessRightToLowException | NotFoundException e) {
      LOG.info("Error in retrieving data: {} -> {}", e.getHttpStatus(), e.getMessage());
      status = e.getHttpStatus();
      payloadString = e.getMessage();
    }
    LOG.info("Resulting message size: {}", payloadString.length());
    MultiPartMessage multiPartMessage = new MultiPartMessage.Builder()
        .setHeader(new ArtifactResponseMessageBuilder()
            ._modelVersion_("2.0.0")
            ._issued_(DateUtil.now())
            ._correlationMessage_(header.getId())
            ._issuerConnector_(URI.create(Config.dataApp().getId()))
            ._recipientConnector_(asList(header.getIssuerConnector()))
            .build())
        .setPayload(status.value()+";"+payloadString)
        .build();
    return MultiPart.toResponseEntity(multiPartMessage, HttpStatus.OK);
  }
}
