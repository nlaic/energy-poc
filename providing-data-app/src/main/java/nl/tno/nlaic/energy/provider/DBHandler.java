package nl.tno.nlaic.energy.provider;

import nl.tno.nlaic.energy.provider.exceptions.AccessRightToLowException;
import nl.tno.nlaic.energy.provider.exceptions.NotFoundException;
import nl.tno.nlaic.energy.provider.model.AccessRight;
import nl.tno.nlaic.energy.provider.model.AccessRightRepository;
import nl.tno.nlaic.energy.provider.model.Location;
import nl.tno.nlaic.energy.provider.model.LocationRepository;
import nl.tno.nlaic.energy.provider.model.LocationType;
import nl.tno.nlaic.energy.provider.model.LocationTypeRepository;
import nl.tno.nlaic.energy.provider.model.Measurement;
import nl.tno.nlaic.energy.provider.model.MeasurementRepository;
import nl.tno.nlaic.energy.provider.model.MeasurementType;
import nl.tno.nlaic.energy.provider.model.MeasurementTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * This is the Handler for the Mongo DB requests
 */

@Component
public class DBHandler {
    
    @Autowired
    private AccessRightRepository arRepository;
    @Autowired
    private LocationRepository lRepository;
    @Autowired
    private LocationTypeRepository ltRepository;
    @Autowired
    private MeasurementRepository mRepository;
    @Autowired
    private MeasurementTypeRepository mtRepository; 

    // Locations
    public List<Location> getLocations(AccessRight accessRight) {
        List<LocationType> locationTypes = ltRepository.findByAccessRightIn(getAccessRights(accessRight));
        return lRepository.findByLocationTypeIn(locationTypes); 
    }

    public Location getLocationById(AccessRight accessRight, String id) throws AccessRightToLowException, NotFoundException {

        Location location = lRepository.findById(id).orElse(null);
        if (location == null) {
            throw new NotFoundException("Location with id " + id + " not found");
        }
        if (accessRight.getAccessLevel() < location.getLocationType().getAccessRight().getAccessLevel()) {
            throw new AccessRightToLowException(this.getRequiredRight(location.getLocationType().getAccessRight()));
        }
        return location;
    }

    public List<Location> getLocationsByParent(AccessRight accessRight, String id) throws AccessRightToLowException, NotFoundException {

        Location location = lRepository.findById(id).orElse(null);
        if (location == null) {
            throw new NotFoundException("Location with id " + id + " not found");
        }
        List<Location> children = lRepository.findByParent(location);
        if (children.size() == 0) {
            throw new NotFoundException("Location with id " + id + " has no children");
        }
        if (accessRight.getAccessLevel() < children.get(0).getLocationType().getAccessRight().getAccessLevel()) {
            throw new AccessRightToLowException(this.getRequiredRight(children.get(0).getLocationType().getAccessRight()));
        }
        return children;
    }

    public List<Location> getLocationsByLocationType(AccessRight accessRight, LocationType locationType) throws AccessRightToLowException {
        List<Location> locations = lRepository.findByLocationType(locationType);
        return locations;
    }

    //LocationTypes
    public List<LocationType> getLocationTypes(AccessRight accessRight) {
        List<AccessRight> accessRights = getAccessRightsBelow(accessRight);
        accessRights.add(accessRight);
        return ltRepository.findByAccessRightIn(accessRights);
    }

    public LocationType getLocationTypeById(AccessRight accessRight, String id) throws AccessRightToLowException, NotFoundException {
        LocationType locationType = ltRepository.findById(id).orElse(null);
        if (locationType == null) {
            throw new NotFoundException("Locationtype with id " + id + " is not found");
        }
        if (accessRight.getAccessLevel() < locationType.getAccessRight().getAccessLevel()) {
            throw new AccessRightToLowException(this.getRequiredRight(locationType.getAccessRight()));  
        }
        return locationType;
    }

    //Measurements
    public List<Measurement> getMeasurementsByLocations(AccessRight accessRight, List<Location> locations) {
        List<MeasurementType> mTypes = mtRepository.findByAccessRightIn(this.getAccessRights(accessRight));
        return mRepository.findByLocationInAndMeasurementTypeIn(locations, mTypes);
    }

    public List<Measurement> getMeasurementsByLocation(AccessRight accessRight, Location location) {
        List<MeasurementType> mTypes = mtRepository.findByAccessRightIn(this.getAccessRights(accessRight));
        return mRepository.findByLocationAndMeasurementTypeIn(location, mTypes);
    }

    // MeasurementTypes
    public List<MeasurementType> getMeasurementTypes(AccessRight accessRight) {

        return mtRepository.findByAccessRightIn(this.getAccessRights(accessRight));
    }

    //AccessRights
    public AccessRight getFirstAccessRightByName(String name) {
        return arRepository.findByName(name).get(0);
    }

    public  List<AccessRight> getAllAccessRights() {
        return arRepository.findAll();
    }
  
    // Private methods
    private List<AccessRight> getAccessRights(AccessRight accessRight) {
        List<AccessRight> accessRights =  getAccessRightsBelow(accessRight);
        accessRights.add(accessRight);
        return accessRights;
    }

    
    private String getRequiredRight(AccessRight accessRight)
    {
        String rightNeeded = "For this data one of the following identites is needed: " + accessRight.getName();
        List<AccessRight> accessRights = arRepository
            .findByAccessLevelGreaterThan(accessRight.getAccessLevel());
        for (AccessRight needRight : accessRights) {
          rightNeeded += ", " + needRight.getName() ;
        }
        return rightNeeded;
    }

    private List<AccessRight> getAccessRightsBelow(AccessRight accessRight) {
        return arRepository.findByAccessLevelLessThan(accessRight.getAccessLevel());
    }

    

}
