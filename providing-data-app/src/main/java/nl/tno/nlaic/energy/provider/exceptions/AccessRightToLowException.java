package nl.tno.nlaic.energy.provider.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Exception to indicate the required indentification is not used for the requested data
 */
public class AccessRightToLowException extends CustomException{

    public AccessRightToLowException(String errorMessage) {
        super(errorMessage);
        status = HttpStatus.UNAUTHORIZED;
    }
    
}
