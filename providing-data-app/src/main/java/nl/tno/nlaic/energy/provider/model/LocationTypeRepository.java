package nl.tno.nlaic.energy.provider.model;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LocationTypeRepository extends MongoRepository<LocationType, String> {

    public List<LocationType> findAll();

    public List<LocationType> findByAccessRightIn(List<AccessRight> accessRights);

    public List<LocationType> findByName(String name);

}