package nl.tno.nlaic.energy.provider.model;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MeasurementRepository extends MongoRepository<Measurement, String> {

    public List<Measurement> findByLocation(Location location);

    public List<Measurement> findByMeasurementType(MeasurementType measurementType);

    public List<Measurement> findByLocationAndMeasurementType(Location location , MeasurementType measurementType);

    public List<Measurement> findByLocationAndMeasurementTypeIn(Location location,
            List<MeasurementType> measurementTypes);

    public List<Measurement> findByLocationInAndMeasurementTypeIn(List<Location> locations,
            List<MeasurementType> measurementTypes);
        
    public List<Measurement> findByLocationIn(List<Location> locations);

}