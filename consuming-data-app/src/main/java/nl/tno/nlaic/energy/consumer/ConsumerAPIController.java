package nl.tno.nlaic.energy.consumer;

import de.fraunhofer.iais.eis.ArtifactRequestMessage;
import de.fraunhofer.iais.eis.ArtifactRequestMessageBuilder;
import de.fraunhofer.iais.eis.RejectionMessage;
import nl.tno.ids.common.multipart.MultiPartMessage;
import nl.tno.ids.common.serialization.Config;
import nl.tno.ids.common.serialization.DateUtil;
import org.springframework.data.util.Pair;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URI;

import static de.fraunhofer.iais.eis.util.Util.asList;

/**
 * This is a Spring RestController 
 */

@RestController
@RequestMapping(value = "/api")
public class ConsumerAPIController {

  @RequestMapping(value = "/retrieve-data", method = RequestMethod.GET)
  public ResponseEntity retrieveData(@RequestParam String provider, @RequestParam String resource) throws IOException {
    
    //Create an IDS ArtifactRequestMessage containing the message destination and the resource to request
    ArtifactRequestMessage artifactRequestMessage = new ArtifactRequestMessageBuilder()
        ._modelVersion_("2.0.0")
        ._issued_(DateUtil.now())
        ._issuerConnector_(URI.create(Config.dataApp().getId()))
        ._recipientConnector_(asList(URI.create(provider)))
        ._requestedArtifact_(URI.create(resource))
        .build();

    // Send the message and receive a Multipart Message as a response
    Pair<Integer, MultiPartMessage> result = IDSController.sendHTTP(provider, artifactRequestMessage, "");

    if (result.getSecond().getHeader() instanceof RejectionMessage) {
      return ResponseEntity.status(500).body(result.getSecond().getPayload());
    }
    int status = Integer.parseInt(result.getSecond().getPayload().substring(0,3));
    return ResponseEntity.status(status).body(result.getSecond().getPayload().substring(4));

  }

}
