package nl.tno.nlaic.energy.consumer;

import brave.Tracing;
import brave.httpclient.TracingHttpClientBuilder;
import nl.tno.ids.base.HttpHelper;
import nl.tno.ids.base.IDSRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class IDSController extends IDSRestController {
    @Autowired(required = false)
    private Tracing tracing;

    private static final Logger LOG = LoggerFactory.getLogger(IDSController.class);

    @PostConstruct
    public void postConstruct() {
        LOG.info("Creating tracing httpclient");
        if (tracing == null) {
            tracing = Tracing.newBuilder().build();
        }
        HttpHelper.getHttpClient(TracingHttpClientBuilder.create(tracing));
        LOG.info("Created tracing httpclient");
    }
}
