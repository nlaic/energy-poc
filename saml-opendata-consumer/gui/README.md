# IOS FL Demo GUI

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

Federated learning Workbook needs to be accessible at port 8081 for the same hostname as the NodeJS server


### Docker Image usage
```
docker run -it --rm -p 8080:80 -e API_BACKEND=192.168.44.137:8081/api registry.ids.smart-connected.nl/ios-researcher-gui
```
