# SAML / OpenData Consumer

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

This repository contains Javascript/Typescript code that implements an SAML / Opendata consumer UI and API.

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

- [GUI](#gui)
  - [SAML login](#saml-login)
- [API](#api)
- [Docker](#docker)
  - [Building the image](#building the image)  
  - [Running the image](#running-the-image)

## GUI

The GUI aalows the user to drill down into location and energy consumption data.
By clicking on the <imf src="img/bt_rtrv_loactions.png" width="75px"> button , the location type the identity has access to will be displayed. By using different types of identification, different levels of depth in the data will be displayed.

<img src="img/GUI1.png" width=50%><img src="img/GUI2.png" width=50%>

### SAML login

In order to get data from a SAML enabled provider, the <img src="img/bt_login.png" width="75px"> needs to be used and defined credentials need to be given. 

In case the default SAML-IDP image used in the helm-charts is used the followin credentials can be used :

- user: test_user
- password: test password  (notice the space between test and password)

<img src="img/SAML_login.png" width=50%>

## API

The GUI uses a backend API to communicate with the providers directly for SAML or OpenData, or the consume IDS Connector for IDS enabled providers. This API will also store the SAML assertion for re-use after login.
