const express = require('express');
const axios = require('axios');
const path = require('path');
const randomId = require('random-id');
const app = express(),
    bodyParser = require("body-parser");

const dataAppEndpoint = process.env.DATA_APP_ENDPOINT || ""
const idsProviders = process.env.IDS_PROVIDERS || ""
const samlProviders = process.env.SAML_PROVIDERS || ""
const openDataProviders = process.env.OPEN_DATA_PROVIDERS || ""

port = 8080;

let providers = {}
idsProviders.split(';').forEach(provider => {
    if (provider.trim() !== '') {
        providers[provider] = {
            type: 'IDS'
        }
    }
})
samlProviders.split(';').forEach(provider => {
    if (provider.trim() !== '') {
        providers[provider] = {
            type: 'SAML',
            token: undefined
        }
    }
});
openDataProviders.split(';').forEach(provider => {
    if (provider.trim() !== '') {
        providers[provider] = {
            type: 'OPEN_DATA'
        }
    }
})


app.use(function(req, res, next) {
    if (req.query.token !== undefined && req.query.provider !== undefined) {
        providers[req.query.provider].token = req.query.token
        console.log(`Successfully set token to ${req.query.token} for provider ${req.query.provider}`)
        res.redirect('/')
    } else {
        next()
    }
})

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../gui/dist')));

app.get('/providers', async(req, res) => {
    res.send(providers)
})

app.get('/data', async(req, res) => {
    const provider = req.query.provider;
    const resource = req.query.resource;

    const options = {};
    let url;
    if (providers[provider] !== undefined) {
        switch (providers[provider].type) {
            case 'IDS':
                url = `${dataAppEndpoint}/query`
                options.params = {
                    provider: provider,
                    resource: resource
                }
                break;
            case 'SAML':
                url = `https://${provider}/saml/${resource}`
                if (providers[provider].token !== undefined) {
                    options.headers = {
                        'Authorization': `Bearer ${providers[provider].token}`
                    }
                }
                break;
            case 'OPEN_DATA':
                url = `https://${provider}/opendata/${resource}`
                break;
            default:
                res.status(400)
                return;
        }
    }
    try {
        const data = await axios.get(url, options)
        res.set('Content-Type', data.headers['content-type'] || 'text/plain');
        res.send(data.data);
    } catch (error) {
        if (error.response) {
            res.status(error.response.status);
            res.send(error.response.data);
        } else {
            res.status(400);
            res.send();
            console.log(error)
        }
    }
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../gui/dist/index.html'));
});

app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});