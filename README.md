# NL AIC: Energy PoC

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

>*NOTE:* This project is part of a Proof of Concept and therefore does not contain production ready code!

- [Hybrid data sharing environments](#hybrid-data-sharing-environments)
- [Project structure](#project-structure)
- [Running the demo](#running-the-demo)
- [Enexis Dataset](#enexis-dataset)
  - [Dataset Structure](#dataset-structure)
  - [Access levels](#access-levels)
- [SAML](#saml)
- [Technical Components](#technical-components)
- [Known issues](#known-issues)

This project contains the documentation and code for a Proof of Concept for the Energy sector, initiated by the Dutch AI Coalition [(NL-AIC)](https://nlaic.com/).

## Hybrid data sharing environments

The hybrid data sharing environment poses challenges on interworking in situations in which Data Service Providers and Data Service Consumers have different capabilities. The main goal of this part of the PoC is to demonstrate the possibilities for data sharing for cases in which the Data Service Provider and Data Service Consumer have different data sharing building blocks with respect to identity and authentication.

In this PoC 3 types of identities are used:

- IDS, using a hybrid IDS/SAML connector but only using the IDS routing.
- SAML, using a hybrid IDS/SAML connector but only using the SAML routing.
- Opendata, no identity needed, the data is offered without security.


## Project structure

The PoC consists of several microservices (further explanation [here](#technical-components)).
Some of those services are located as subfolders in this project:

- [consuming-data-app](./consuming-data-app/README.md) contains the Java / JScript code for the consuming data app. This data app uses IDS components
- [providing-data-app](./providing-data-app/README.md) contains the Java code for the providing data app and the providing data connector. This data-app will be used for both data providers
- [helm-chart](./helm-chart/README.md) contains the Helm code used for deploying the services to a Kubernetes cluster.
- [saml-opendata-consumer](./saml-opendata-consumer/README.md) contains the SAML/Opendata consumer data app and UI. This project does not contain IDS components

## Running the demo

No preperation is needed to run the demo. The [helm-chart](/helm-chart/README.md) will deploy the demo on a kubernetes cluster. There is no action needed after deploying the [helm-chart](/helm-chart/README.md)

## Enexis Dataset

The dataset used in this PoC is an opendata set provided by [Enexis](https://www.enexis.nl/over-ons/wat-bieden-we/andere-diensten/open-data) and is integrated into the Provider-data-app. Not all data from the Enexis dataset is used and some data is added via a random value. The dataset is translated to the structure described at [Dataset Structure](#structure). 

The location information is provided through the *BAG* data provider. The energy usage data is provided by the *Netbeheerder* data provider.

### Dataset Structure

![dataset_structure](img/dataset_structure.svg)

The data is communicated in json

### Access levels

The dataset id divided in three levels of access:

- IDS (lvl 3)  (also has access to SAML and OPENDATA)
- SAML (lvl 2) (also has access to OPENDATA)
- Opendata (lvl 1)

Based on the connector type (using the routing and corresponding handler in the data app) the data corresponding with the access level is provided or access is refused.

Location data is divided into:

| Locationtype | access level  |
| :-:          | :-:           |
| country      | Opendata      |
| city         | Opendata      |
| street       | IDS           |
| postalrange  | IDS           |

Energy consumption data is divided into:

| Measurementtype        | access level  |
| :-:                    | :-:           |
| ELK_SJV_AVERAGE        | SAML          |
| GAS_SJV_AVERAGE        | Opendata      |
| ELK_SJV_LOW_TARIF_PERC | IDS           |

## SAML

One of the IdP's used is [SAML](https://en.wikipedia.org/wiki/SAML_2.0) based.
SAML required the consumer to be authenticated by a SAML IdP and receive an assertion to communicate wiht the data provider service. Before being able to access data through the SAML based consumer the user is able to login with the SAML Idp and obtain a SAML assertion. This assertion is then used to send requests to the hybrid IDS Connector to identify and authenticate the consumer.

For this PoC the following credentials are used:

- user: test-user
- password: test password (!Notice the space between test and password)

The SAML IdP is based on the [laa-saml-mock](https://github.com/ministryofjustice/laa-saml-mock) provided through [github](https://github.com) by the UK Ministry of Justice.

The helm-chart uses a simplified version of this SAML IdP, for which a docker image is provided

## Technical Components

The PoC is based on the [IDS reference architecture](https://www.internationaldataspaces.org/wp-content/uploads/2019/03/IDS-Reference-Architecture-Model-3.0.pdf).
Several connectors and a mongoDB instance are deployed on a kubernetes cluster.

![components](img/components.svg)

- Data consumer 1, 2 and 3. Each with their own Identity (None, SAML or IDS).
- *BAG Data Provider*: The BAG data service, providing location information using IDS and Opendata (no security)
- *Netbeheerder Data Provider*: The Netbeheerder data service, providing energy consumption data using SAML and Opendata (no security)

## Known issues

There are no known issues at this time.
